﻿using System;
using System.Collections.Generic;

namespace Notebook
{
    class NoteBook
    {
        public static List<Record> records = new List<Record>();

        static void Main(string[] args)
        {
            Console.WriteLine("Привет, пользователь! Я - приложение, созданное специально для записи всех твоих друзей (и врагов) с коронавирусом. " +
                "Тебе лучше держаться от них подальше, но ты можешь пожелать им скорейшего выздоровления по телефону:)\n");
            Console.WriteLine("Я умею создавать, хранить и редактировать контакты \"опасных\" людей. Ты всегда сможешь посмотреть свой список, а также, если кто-нибудь " +
                "из твоих товарищей выздоровит, удалить контакт.\n");
            Console.WriteLine("Итак, даю тебе на выбор команды, выбирай, что хочешь. Когда определишься, набери номер команды.\n");
            Interface();
        }
        public static void Interface()
        {
            int command = new int();
            
            do
            {
                Console.WriteLine("1. Добавить контакт\n2. Редактировать контакт\n3. Удалить контакт\n4. Посмотреть список контактов\n5. Выход");
                int.TryParse(Console.ReadLine(), out command);
                switch (command)
                {
                    case 1: Console.WriteLine("\nВыбрана команда: добавить контакт\n");
                        Record.Adding();
                        break;
                    case 2: Console.WriteLine("\nВыбрана команда: редактировать контакт\n");
                        Record.EditContact();
                        break;
                    case 3: Console.WriteLine("\nВыбрана команда: удалить контакт\n");
                        Record.Deleter();
                        break;
                    case 4: Console.WriteLine("\nВыбрана команда: посмотреть список контактов\n");
                        Record.Reviewer();
                        break;
                    case 5: Console.WriteLine("\nВыбрана команда: выход\n");
                        break;
                    default: Console.WriteLine("\nА теперь нормально введи\n");
                        break;
                }
            } while (command!=5);
        }
    }
}
