﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Notebook
{
    class Record
    {

        private string surname;
        private string name;
        private string fathername; //optional
        private string number;
        private string country;
        private string birthsday; //optional
        private string organization; //optional
        private string position; //optional
        private string otherInf; //optional
        private int id;

        static string InicializationForNeccessary()
        {
            string s;
            do
            {
                s = Console.ReadLine();
                if (!String.IsNullOrEmpty(s))
                {
                    return s;
                }
                Console.WriteLine("Похоже вы ничего не ввели");
                Console.WriteLine("Попробуйте ещё раз");
            } while (true);
        }
        public static void EditContact()
        {
            if (NoteBook.records.Count > 0)
            {
                Console.WriteLine("\nВыберите контакт, который хотите отредактировать (мало ли человек не сидит дома или любит изменять имя)\n");
                foreach (var item in NoteBook.records)
                {
                    Console.WriteLine(item.id + ". " + item.surname + " " + item.name);
                }
                Console.WriteLine(NoteBook.records.Count+1 +". Выход");
                int selectedContact;
                Record contact = new Record();
                do
                {
                    int.TryParse(Console.ReadLine(), out selectedContact);
                    if (selectedContact >= 1 & selectedContact <= NoteBook.records.Count)
                    {
                        contact = NoteBook.records[selectedContact - 1];
                    }
                    else
                    {
                        if (selectedContact != NoteBook.records.Count + 1)
                        {
                            Console.WriteLine("Похоже вы промахнулись мимо нужной кнопки)");
                        }
                    }
                }
                while (selectedContact < 1 || selectedContact > NoteBook.records.Count+1);
                int selectedField;
                if (selectedContact != NoteBook.records.Count + 1)
                {
                    Console.WriteLine("\nВыберите поле, которое хотите отредактировать:\n");
                    Console.WriteLine("1. Имя\n2. Фамилия\n3. Отчество\n4. Номер\n5. Страна\n6. Дата рождения\n7. Место работы\n8. Должность\n9. Другая информация\n10. Выход");
                    do
                    {
                        int.TryParse(Console.ReadLine(), out selectedField);
                        switch (selectedField)
                        {
                            case 1:
                                Console.WriteLine("Введите новое имя:");
                                contact.name = InicializationForNeccessary();
                                break;
                            case 2:
                                Console.WriteLine("Введите новую фамилию:");
                                contact.surname = InicializationForNeccessary();
                                break;
                            case 3:
                                Console.WriteLine("Введите новое отчество:");
                                contact.fathername = Console.ReadLine();
                                break;
                            case 4:
                                Console.WriteLine("Введите новый номер:");
                                contact.number = InicializationForNeccessary();
                                break;
                            case 5:
                                Console.WriteLine("Введите новую страну:");
                                contact.country = InicializationForNeccessary();
                                break;
                            case 6:
                                Console.WriteLine("Введите новую дату рождения:");
                                contact.birthsday = Console.ReadLine();
                                break;
                            case 7:
                                Console.WriteLine("Введите новое место работы:");
                                contact.organization = Console.ReadLine();
                                break;
                            case 8:
                                Console.WriteLine("Введите новую должность:");
                                contact.position = Console.ReadLine();
                                break;
                            case 9:
                                Console.WriteLine("Введите новую дополнительную информацию:");
                                contact.otherInf = Console.ReadLine();
                                break;
                            case 10: break;

                            default:
                                Console.WriteLine("\nОшибка: введен бред\n");
                                break;
                        }
                    }
                    while (selectedField < 1 || selectedContact > 10);
                }
            }
            else
            {
                Console.WriteLine("Похоже все ваши друзья здоровы и редактировать некого)\n");
            }
        }

        public static void Adding()
        {
            Record rec = new Record();
            rec.id = NoteBook.records.Count + 1;
            Console.WriteLine("Введите имя:");
            rec.name = InicializationForNeccessary();
            Console.WriteLine("Введите фамилию:");
            rec.surname = InicializationForNeccessary();
            Console.WriteLine("Введите отчество (если человек Казах, оставьте поле пустым):");
            rec.fathername = Console.ReadLine();
            Console.WriteLine("Введите номер (рассчитываю на вашу честность):");
            rec.number = InicializationForNeccessary();
            Console.WriteLine("Введите страну:");
            rec.country = InicializationForNeccessary();
            Console.WriteLine("Введите дату рождения(если не знаете, оставьте поле пустым):");
            rec.birthsday = Console.ReadLine();
            Console.WriteLine("Введите место работы (если человек безработный оставьте поле пустым):");
            rec.organization = Console.ReadLine();
            Console.WriteLine("Введите должность на работе (если человек на карантине оставьте поле пустым):");
            rec.position = Console.ReadLine();
            Console.WriteLine("Введите другую информацию (если нечего сказать оставьте поле пустым):");
            rec.otherInf = Console.ReadLine();
            NoteBook.records.Add(rec);
            Console.WriteLine("\nКонтакт добавлен!");
        }
        public static void Deleter()
        {
            if (NoteBook.records.Count == 0)
            {
                Console.WriteLine("\nВсе твои друзья здоровы (ну или крысы)\n");
            }
            else
            {
                Console.WriteLine("\nВыберите контакт, который хотите удалить (мало ли вылечился)\n");
                foreach (var item in NoteBook.records)
                {
                    Console.WriteLine(item.id + ". " + item.surname + " " + item.name);
                }
                Console.WriteLine(NoteBook.records.Count+1 + ". Выход");
                int selectedContact;
                Record contact = new Record();
                bool kek = false;
                do
                {
                    kek = int.TryParse(Console.ReadLine(), out selectedContact);
                    if (selectedContact != NoteBook.records.Count + 1)
                    {
                        if (selectedContact >= 1 & selectedContact <= NoteBook.records.Count)
                        {
                            contact = NoteBook.records[selectedContact - 1];
                        }
                        else
                        {
                            Console.WriteLine("\nПохоже вы хотите удалить несуществующего человека\n");
                        }
                    }
                }
                while ((selectedContact < 1 || selectedContact > NoteBook.records.Count+1)&& kek == false );
                if (selectedContact != NoteBook.records.Count + 1)
                {
                    Console.WriteLine("\nВы уверены?\n");
                    Console.WriteLine("1. Да\n2. Нет\n");
                    kek = false;
                    int yesno;
                    do
                    {
                        kek = int.TryParse(Console.ReadLine(), out yesno);
                        switch (yesno)
                        {
                            case 1:
                                NoteBook.records.Remove(contact);
                                for (int i = 1; i < NoteBook.records.Count + 1; i++)
                                {
                                    if (NoteBook.records[i].id != i)
                                    {
                                        NoteBook.records[i].id = i;
                                    }
                                }
                                Console.WriteLine("\nКонтакт удален\n");
                                break;
                            case 2:
                                Console.WriteLine("\nНу нет и нет, чо бубнить\n");
                                break;
                            default:
                                Console.WriteLine("\nРазберись в себе\n");
                                break;
                        }
                    }
                    while (yesno > 2 | yesno < 1);
                }
            }
        }
        public static void Reviewer()
        {
            if (NoteBook.records.Count == 0)
            {
                Console.WriteLine("\nВсе твои друзья здоровы (ну или крысы)\n");
            }
            else
            {
                Console.WriteLine("\nВот все, кого стоит избегать. Если хотите узнать поподробнее о человеке, ткните нужную цифру:\n");
                foreach (var item in NoteBook.records)
                {
                    Console.WriteLine(item.id + ". " + item.name + " " + item.surname + " Num: " + item.number);
                }
                Console.WriteLine(NoteBook.records.Count + 1 + ". Выход");
                int choice;
                bool kek;
                do
                {
                    kek = int.TryParse(Console.ReadLine(), out choice);
                    choice -= 1;
                    switch (choice)
                    {
                        case int k when (k>=0 & k<NoteBook.records.Count):
                            Console.WriteLine($"1. Имя: {NoteBook.records[choice].name}\n2. Фамилия: {NoteBook.records[choice].surname}\n3. Отчество: {NoteBook.records[choice].fathername}" +
                                $"\n4. Номер: {NoteBook.records[choice].number}\n5. Страна: {NoteBook.records[choice].country}\n6. Дата рождения: {NoteBook.records[choice].birthsday}" +
                                $"\n7. Место работы: {NoteBook.records[choice].organization}\n8. Должность: {NoteBook.records[choice].position}\n" +
                                $"9. Другая информация: {NoteBook.records[choice].otherInf}\n");
                            break;
                        case int k when (k == NoteBook.records.Count):
                           break;
                        default:
                            Console.WriteLine("\nПеревыбери\n");
                            break;
                    }
                } while (choice<0 | choice> NoteBook.records.Count);

            }
        }
    }
}
